package com.ssdigital.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Sistema")
public class Sistema {
	@Id
	@GeneratedValue
	private int id;
	private String descricao;
	private String sigla;
	private String emailDeAtendimento;
	private String url;

	private String status;
	private String ultimaAlteracao;
	private Timestamp dataUltimaAlteracao;
	private String justificativa;

	public Sistema() {

	}

	public Sistema(String descricao, String sigla, String emailDeAntendimento, String url) {
		this.descricao = descricao;
		this.sigla = sigla;
		this.emailDeAtendimento = emailDeAntendimento;
		this.url = url;
		this.setStatus("ativo");
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "ultimaAlteracao")
	public String getUltimaAlteracao() {
		return ultimaAlteracao;
	}

	public void setUltimaAlteracao(String ultimaAlteracao) {
		this.ultimaAlteracao = ultimaAlteracao;
	}
	
	
	
	@Column(name = "dataUltimaAlteracao", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	public Timestamp getDataUltimaAlteracao() {
		return dataUltimaAlteracao;
	}

	public void setDataUltimaAlteracao(Timestamp date) {
		this.dataUltimaAlteracao = date;
	}

	@Column(name = "justificativa")
	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "descricao")
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Column(name = "sigla")
	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	@Column(name = "emailAtendimento")
	public String getEmailDeAtendimento() {
		return emailDeAtendimento;
	}

	public void setEmailDeAtendimento(String emailDeAtendimento) {
		this.emailDeAtendimento = emailDeAtendimento;
	}

	@Column(name = "url")
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "Sistema [id=" + id + ", descricao=" + descricao + ", sigla=" + sigla + ", emailDeAtendimento="
				+ emailDeAtendimento + ", url=" + url + ", status=" + status + ", ultimaAlteracao=" + ultimaAlteracao
				+ ", dataUltimaAlteracao=" + dataUltimaAlteracao + ", justificativa=" + justificativa + "]\n";
	}

}

package com.ssdigital.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.ssdigital.model.Sistema;
import com.ssdigital.util.HibernateUtil;

public class SistemaDAO {

	/**
	 * Cria um novo registro de sistema
	 * 
	 * @param sistema
	 * @throws Exception
	 */
	public Sistema createSistema(Sistema sistema) throws Exception {
		Transaction trns = null;

		Session session = HibernateUtil.getSessionFactory().openSession();

		try {
			trns = session.beginTransaction();
			session.save(sistema);
			session.getTransaction().commit();

			return sistema;

		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
			throw new Exception("Error ao criar sistema");
		} finally {
			session.flush();
			session.close();
		}

	}

	/**
	 * Obtem um sistema de id específico
	 * 
	 * @param id
	 * @return
	 */
	public Sistema getSistema(int id) {

		Sistema sistema = null;

		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			sistema = (Sistema) session.get(Sistema.class, id);
		} finally {
			session.flush();
			session.close();
		}

		return sistema;
	}

	/**
	 * Lista todos os sistemas cadastrados
	 * 
	 * @return sistemas
	 */
	@SuppressWarnings("unchecked")
	public List<Sistema> listSistemas() {
		List<Sistema> sistemas = new ArrayList<Sistema>();

		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			sistemas = session.createQuery("from Sistema").list();

		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		return sistemas;
	}

	/**
	 * Deleta sistema de a partir de id especificado
	 * 
	 * @param id
	 * @throws Exception
	 */
	public boolean deleteSistema(int id) throws Exception {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			trns = session.beginTransaction();
			Sistema sistema = (Sistema) session.load(Sistema.class, id);

			session.delete(sistema);
			session.getTransaction().commit();

			return true;

		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
			throw new Exception("Error ao excluir sistema");
		} finally {
			session.flush();
			session.close();
		}

	}

	/**
	 * Atualiza dados de um determinado sistema
	 * 
	 * @param sistema
	 * @throws Exception
	 */
	public boolean updateSistema(Sistema sistema) throws Exception {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			trns = session.beginTransaction();
			session.update(sistema);
			session.getTransaction().commit();
			return true;
		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
			throw new Exception("Error ao atualizar sistema");
		} finally {
			session.flush();
			session.close();
		}
	}

	/**
	 * Pesquisa sistema com base nas entradas especificadas
	 * 
	 * @param descricao
	 * @param sigla
	 * @param emailDeAtendimento
	 * @return sistemas
	 */
	@SuppressWarnings("unchecked")
	public List<Sistema> pesquisa(Sistema sistema) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		// String hql = "from Sistema s where s.descricao like :descricao";

		// String hql = "from Sistema s where s.descricao like :descricao" + "
		// and s.sigla like :sigla"
		// + " and s.emailDeAtendimento like :emailDeAtendimento";

		List<Sistema> sistemas = null;

		
		//sistema.setSigla(sistema.getSigla() + "%");
		//sistema.setEmailDeAtendimento(sistema.getEmailDeAtendimento() + "%");

		try {

			if (!(sistema.getDescricao().isEmpty()) && sistema.getSigla().isEmpty()
					&& sistema.getEmailDeAtendimento().isEmpty()) {
				String hql = "from Sistema s where s.descricao like :descricao";
				sistema.setDescricao(sistema.getDescricao() + "%");
				sistemas = session.createQuery(hql).setParameter("descricao", sistema.getDescricao()).list();
				sistema.setDescricao(sistema.getDescricao().replace("%", ""));
			}
			
			if(!(sistema.getSigla().isEmpty()) && sistema.getDescricao().isEmpty() && sistema.getEmailDeAtendimento().isEmpty()){
				String hql = "from Sistema s where s.sigla like :sigla";
				sistema.setSigla(sistema.getSigla() + "%");
				sistemas = session.createQuery(hql).setParameter("sigla", sistema.getSigla()).list();
				sistema.setSigla(sistema.getSigla().replace("%", ""));
			}
			
			if(!(sistema.getEmailDeAtendimento().isEmpty())&& sistema.getDescricao().isEmpty() && sistema.getSigla().isEmpty()){
				String hql = "from Sistema s where s.emailDeAtendimento like :emailDeAtendimento";
				sistema.setEmailDeAtendimento(sistema.getEmailDeAtendimento()+"%");
				sistemas = session.createQuery(hql).setParameter("emailDeAtendimento", sistema.getEmailDeAtendimento()).list();
				sistema.setEmailDeAtendimento(sistema.getEmailDeAtendimento().replace("%", ""));
			}

		} finally {
			session.flush();
			session.close();
		}

		return sistemas;
	}

}

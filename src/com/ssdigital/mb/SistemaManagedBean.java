package com.ssdigital.mb;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.faces.bean.ManagedBean;

import com.ssdigital.dao.SistemaDAO;
import com.ssdigital.model.Sistema;

@ManagedBean(name = "sistemaMB")
public class SistemaManagedBean {

	private Sistema sistema = new Sistema();
	private List<Sistema> sistemas;
	private String msg;
	private String selectedId;
	private String novaJustificativa;

	public String getNovaJustificativa() {
		return novaJustificativa;
	}

	public void setNovaJustificativa(String novaJustificativa) {
		this.novaJustificativa = novaJustificativa;
	}

	public String getSelectedId() {
		return selectedId;
	}

	public void setSelectedId(String selectedId) {
		this.selectedId = selectedId;
	}

	public void setSistemas(List<Sistema> sistemas) {
		this.sistemas = sistemas;
	}

	public Sistema getSistema() {
		return sistema;
	}

	public void setSistema(Sistema sistema) {
		this.sistema = sistema;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * Limpa dados do sistema
	 */
	public void limparSistema() {

		this.sistema.setDataUltimaAlteracao(null);
		this.sistema.setDescricao("");
		this.sistema.setEmailDeAtendimento("");
		this.sistema.setId(0);
		this.sistema.setJustificativa("");
		this.sistema.setSigla("");
		this.sistema.setStatus("");
		this.sistema.setUltimaAlteracao("");
		this.sistema.setUrl("");
		this.setNovaJustificativa("");
		this.setSelectedId("");
	}
	
	
	/**
	 * limpa registro de sistema para novo cadastro
	 * @return str
	 */
	public String novoSistema(){
		
		String str = "insert";
		
		limparSistema();
		this.setMsg("");
		
		return str;
	}

	/**
	 * Cadastra sistema
	 * 
	 * @param sistema
	 * @return str
	 */
	public String salva(Sistema sistema) {

		String str = "insert";
		try {
			sistema.setStatus("ativo");
			new SistemaDAO().createSistema(sistema);
			limparSistema();
			this.setMsg("Operação realizada com sucesso!");
			this.setSistemas(new SistemaDAO().listSistemas());

		} catch (Exception e) {
			this.setMsg(e.getMessage());
			str = "insert";
		}

		return str;
	}

	/**
	 * Obtem lista de todos os sistemas
	 * 
	 * @return sistemas
	 */
	public List<Sistema> getSistemas() {

		if (this.sistemas == null)
			sistemas = new SistemaDAO().listSistemas();

		return sistemas;
	}

	/**
	 * preenche dados de sistema para alteração
	 * 
	 * @return "update"
	 */
	public String editSistema() {

		Sistema sistema = new SistemaDAO().getSistema(Integer.parseInt(this.getSelectedId()));

		if (sistema != null) {

			this.sistema.setDescricao(sistema.getDescricao());
			this.sistema.setEmailDeAtendimento(sistema.getEmailDeAtendimento());
			this.sistema.setUrl(sistema.getUrl());
			this.sistema.setDataUltimaAlteracao(sistema.getDataUltimaAlteracao());
			this.sistema.setJustificativa(sistema.getJustificativa());
			this.sistema.setStatus(sistema.getStatus());
			this.sistema.setUltimaAlteracao(sistema.getUltimaAlteracao());
			this.sistema.setSigla(sistema.getSigla());
			this.setMsg("");

		} else {
			this.setMsg("Sistema nao encontrado!");

		}

		return "update";
	}

	/**
	 * Altera sistema
	 * 
	 * @param sistema
	 * @return str
	 */
	public String alteraSistema() {
		String str = "index";
		try {

			sistema.setJustificativa(novaJustificativa);

			//long time = System.currentTimeMillis();

			//java.sql.Date date = new java.sql.Date(time);

			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/Sao_Paulo"));
			java.sql.Timestamp date = new Timestamp(cal.getTimeInMillis());
			
			System.out.println(date);
			sistema.setDataUltimaAlteracao(date);

			sistema.setId(Integer.parseInt(selectedId));
			System.out.println(sistema);
			new SistemaDAO().updateSistema(sistema);
			limparSistema();
			this.setMsg("Atualizado com sucesso!");
			this.setSistemas(new SistemaDAO().listSistemas());

		} catch (Exception e) {
			this.setMsg(e.getMessage());
			str = "index";

		}

		return str;
	}
	
	/**
	 * Pesquisa sistema com base nos parametro especificados 
	 * @param sistema
	 * @return str
	 */
	public String pesquisa(Sistema sistema){
		String str ="index";
		List<Sistema> sistemas = new SistemaDAO().pesquisa(sistema);
		
		this.setSistemas(sistemas);
		return str;
	}
	
	/**
	 * Limpa dados da pesquisa
	 * @return str
	 */
	public String limparPesquisa(){
		
		String str = "index";
		limparSistema();
		this.setSistemas(new SistemaDAO().listSistemas());
		return str;
	}
	
	/**
	 * Voltar das telas de insert e update
	 * @return str
	 */
	public String voltar(){
		
		String str = "index";
		limparSistema();
		this.setMsg("");
		return str;
		
	}

}

package com.ssdigital.test;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import com.ssdigital.dao.SistemaDAO;
import com.ssdigital.model.Sistema;

import junit.framework.TestCase;

public class TesteSistemaDAO extends TestCase {
	
	/**
	 * Testa create
	 * @throws Exception
	 */
	public void testeCreate() throws Exception {

		Sistema sistema = new Sistema("AF", "gestao", "f@f", "www.af.com");

		Sistema s = new SistemaDAO().createSistema(sistema);

		assertTrue(s == sistema);

	}
	
	/**
	 * Testa getSistemaId
	 */
	public void testeGetSistemaId() {

		List<Sistema> sistemas = new SistemaDAO().listSistemas();
		Sistema sistema = new SistemaDAO().getSistema(sistemas.get(sistemas.size()-1).getId());

		assertNotNull(sistema);

	}
	
	/**
	 * Testa listSistemas
	 */
	public void testeListSistemas() {

		List<Sistema> sistemas = new SistemaDAO().listSistemas();

		assert (sistemas.size() > 0);
	}
	
	/**
	 * Testa update
	 * @throws Exception
	 */
	public void testeUpdate() throws Exception {
		
		List<Sistema> sistemas = new SistemaDAO().listSistemas();
		
		Sistema sistema = new SistemaDAO().getSistema(sistemas.get(sistemas.size()-1).getId());

		sistema.setDescricao("teste");
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/Sao_Paulo"));
		java.sql.Timestamp date = new Timestamp(cal.getTimeInMillis());
		sistema.setDataUltimaAlteracao(date);
		sistema.setEmailDeAtendimento("teste@teste");
		sistema.setJustificativa("teste unitario");
		sistema.setSigla("TESTE");
		sistema.setStatus("inativo");
		sistema.setUltimaAlteracao("JUNIT TESTE");
		sistema.setUrl("www.teste.com");

		assertTrue(new SistemaDAO().updateSistema(sistema));

	}
	
	/**
	 * Testa delete
	 * @throws Exception
	 */
	public void testeDeleteSistema() throws Exception {
		
		List<Sistema> sistemas = new SistemaDAO().listSistemas();
		assertTrue(new SistemaDAO().deleteSistema(sistemas.get(sistemas.size()-1).getId()));

	}

}
